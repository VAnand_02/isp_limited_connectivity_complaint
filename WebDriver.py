
from sys import platform
from selenium import webdriver


class WebDriver:

    @staticmethod
    def driver():

        if platform == "linux" or platform == "linux2":
            # Enter the path of the browser / driver that you want to use.Currently it's None.
            return None
        elif platform == "darwin":
            return webdriver.Safari(executable_path='/usr/bin/safaridriver')
        elif platform == "win32":
            # Enter the path your IE driver i.e. path for 'IEDriverServer.exe'
            return webdriver.Ie(executable_path='')
