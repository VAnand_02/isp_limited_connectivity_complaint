
# Shebang!
# !/usr/bin/env python

from ISPRaiseComplain import ISPRaiseComplain


def main():
    ISPRaiseComplain.raise_complain()


if __name__ == '__main__':
    main()

