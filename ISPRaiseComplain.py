

import time
from WebDriver import WebDriver
from Reachability import Reachability
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException


class ISPRaiseComplain:

    _driver = None

    @staticmethod
    def driver():
        if not ISPRaiseComplain._driver:
            ISPRaiseComplain._driver = WebDriver.driver()
        return ISPRaiseComplain._driver

    @staticmethod
    def load_with_wait(element_id):
        delay = 4
        driver = ISPRaiseComplain.driver()
        try:
            element = WebDriverWait(driver, delay).until(ec.presence_of_element_located((By.ID, element_id)))
            print('Page is ready')
            return element
        except TimeoutException:
            print('Loading took too much time.')
            driver.quit()

    @staticmethod
    def raise_complain():

        if Reachability.is_connected():

            driver = ISPRaiseComplain.driver()
            login_url = "http://myaccount.gazonindia.com/loginpage.aspx"
            driver.maximize_window()
            driver.get(login_url)
            driver.find_element_by_id('txtUserName').send_keys('')
            driver.find_element_by_id('txtlogPassword').send_keys('')
            driver.find_element_by_id('btnSubmit').click()
            driver.get('http://myaccount.gazonindia.com/Masters/MyProfile.aspx')

            customer_care_button = ISPRaiseComplain.load_with_wait('ctl00_rptmenu_ctl05_lnkMenu')
            customer_care_button.click()

            launch_new_complain_button = ISPRaiseComplain.load_with_wait('ctl00_ContentPlaceHolder1_btnNew')
            launch_new_complain_button.click()

            complain_type_drop_down = ISPRaiseComplain.load_with_wait('ctl00_ContentPlaceHolder1_ddlComplaintType')
            select = Select(complain_type_drop_down)
            select.select_by_visible_text('Limited or No connectivity')

            # Wait for the script to make a selection from the drop down list.
            time.sleep(1)
            comment_text_field = ISPRaiseComplain.load_with_wait('ctl00_ContentPlaceHolder1_txtComment')
            comment_text_field.send_keys('Not able to access internet.')

            # Wait for the script to auto fill the comment field.
            time.sleep(1)
            submit_button = ISPRaiseComplain.load_with_wait('ctl00_ContentPlaceHolder1_btnSave')
            submit_button.click()

            # Wait to check if complain is raised sucessfully or not.
            time.sleep(5)

            driver.quit()

